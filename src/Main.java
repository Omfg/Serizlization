import javax.swing.*;
import java.io.*;
import java.util.ArrayList;

/**
 * Created by omfg on 30.11.16.
 */
public class Main {
    private static ArrayList<Profile>profiles = new ArrayList<Profile>();


    public static void main(String[] args) {
       profiles =(ArrayList<Profile>) dserDate("profiles");
        System.out.println(profiles.size());
      Profile profile = new Profile();
      profile.setName(JOptionPane.showInputDialog(null,"Enter your name"));
      profile.setSurname(JOptionPane.showInputDialog(null,"Enter your surname"));
    profiles.add(profile);
        for (Profile p: profiles ) {
            System.out.println(p.getName()+" "+p.getSurname());
        }
        System.out.println(profiles.size());
        serDate("profiles",profiles);

    }

    private static Object dserDate(String file_name) {
        Object retObject = null;
        try {
            FileInputStream fileIn = new FileInputStream(file_name+".ser");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            retObject = in.readObject();
            fileIn.close();
            in.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println("Class Not Found");
            System.exit(3);
        }
                                return retObject;

    }

    private static void serDate(String file_name,Object obj) {
        try {

            FileOutputStream fileOut = new FileOutputStream(file_name+".ser");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(obj);
            fileOut.close();
            out.close();
        }   catch (FileNotFoundException e){e.printStackTrace();
            System.out.println("File no found");
        System.exit(1);

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("IO ERROR");
            System.exit(2);
        }

    }

}
