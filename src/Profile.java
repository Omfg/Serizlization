import java.io.Serializable;

/**
 * Created by omfg on 30.11.16.
 */
public class Profile implements Serializable {

    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {

        return name;
    }

    public String getSurname() {
        return surname;
    }

    private String surname;

}
